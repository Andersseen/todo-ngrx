import { Component, inject } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { FormControl, ReactiveFormsModule, Validators } from '@angular/forms';
import { addTask, removeAllTasks, resetAllTasks } from '../../store/task.store';
import { Store } from '@ngrx/store';

@Component({
  selector: 'task-form',
  templateUrl: 'form.template.html',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
  ],
  host: { ngSkipHydration: 'true' },
})
export class FormComponent {
  private store = inject(Store);
  public formCtrl = new FormControl(null, [
    Validators.required,
    Validators.minLength(5),
  ]);

  addTask(): void {
    this.store.dispatch(addTask({ title: this.formCtrl.value ?? '' }));
    this.formCtrl.setValue(null);
  }

  clearTasks(): void {
    this.store.dispatch(removeAllTasks());
  }

  resetTasks(): void {
    this.store.dispatch(resetAllTasks());
  }
}
