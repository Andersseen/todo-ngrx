import { Component, Input } from '@angular/core';
import { ITask } from '../../store/task.store';
import {
  CdkDropList,
  CdkDrag,
  CdkDragDrop,
  moveItemInArray,
  CdkDragPlaceholder,
} from '@angular/cdk/drag-drop';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { TaskComponent } from '../task/task.component';

@Component({
  selector: 'task-list',
  templateUrl: 'list.template.html',
  styleUrl: 'list.styles.scss',
  standalone: true,
  imports: [
    CdkDropList,
    CdkDrag,
    CdkDragPlaceholder,
    MatButtonModule,
    MatCardModule,
    MatIconModule,
    TaskComponent,
  ],
})
export class ListComponent {
  public tasks: ITask[] | null = [];
  @Input() set list(value: ITask[]) {
    if (!value) return;
    console.log(value);

    this.tasks = value;
  }

  drop(event: CdkDragDrop<ITask[]>) {
    if (this.tasks) {
      const copiedList = [...this.tasks];
      moveItemInArray(copiedList, event.previousIndex, event.currentIndex);
      this.tasks = copiedList;
    }
  }
}
