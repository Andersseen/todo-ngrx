import { Component, Input, inject } from '@angular/core';
import { Store } from '@ngrx/store';
import { ITask, markTaskAsCompleted, removeTask } from '../../store/task.store';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { CdkDragPlaceholder } from '@angular/cdk/drag-drop';

@Component({
  selector: 'task',
  templateUrl: 'task.template.html',
  styleUrl: 'task.styles.scss',
  standalone: true,
  imports: [MatButtonModule, MatCardModule, MatIconModule, CdkDragPlaceholder],
})
export class TaskComponent {
  private store = inject(Store);
  @Input() task!: ITask;

  markAsCompleted(id: string): void {
    this.store.dispatch(markTaskAsCompleted({ id }));
  }

  removeTask(id: string): void {
    this.store.dispatch(removeTask({ id }));
  }
}
