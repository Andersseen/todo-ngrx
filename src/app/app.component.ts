import { Component, inject } from '@angular/core';
import { ListComponent } from './components/list/list.component';
import { Store } from '@ngrx/store';
import { selectTaks } from './store/task.store';
import { FormComponent } from './components/form/form.compomemt';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
  standalone: true,
  imports: [ListComponent, FormComponent],
})
export class AppComponent {
  private store = inject(Store);
  public tasks = this.store.selectSignal(selectTaks);
}
