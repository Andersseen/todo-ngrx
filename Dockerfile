
FROM node:20-alpine as builder
WORKDIR /app
COPY package*.json ./
RUN npm i -g pnpm && pnpm install
COPY . .
RUN pnpm build

# Etapa de servidor Nginx
FROM nginx:alpine
COPY --from=builder /app/dist/todo-ngrx/browser /usr/share/nginx/html
COPY nginx.conf /etc/nginx/nginx.conf
EXPOSE 4200
CMD ["nginx", "-g", "daemon off;"]

